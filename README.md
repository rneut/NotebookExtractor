# NotebookExtractor

## for quick Jupyter conversions

apply on .ipynk Notebooks to...
- convert formula between Markdown and Code within .ipynb
- convert Code sections to .py
- convert Markdown sections to .tex and insert numeric results, tables and figures

### formula conversion
to convert a formula line, the line before must be respectively: <br>
    ##convert               (in code) <br>
    \<!--- convert -->       (in markdown) <br>
the converted lines will appear at the beginning of the next suitable cell

### result inclusion
to include python results to markdown or LaTeX use this as a placeholder: <br>
    \<!--- val:VALUE --> <br>
    \<!--- rnd:VALUE, UNCERTAINTY --> <br>
    \<!--- tab:ARRAY [*ARGS] -->      (LaTeX conversion only) <br>
    \<!--- mat:ARRAY [*ARGS] -->      (LaTeX conversion only) <br>
figures will be saved and inserted anyway

### modification of tables and matrices
Arguments will be evaluated as python code and are passed as arguments to the code. <br>
most arguments except **val** are optional. It's highly recommended to pass information about uncerainties.
- **val**:        numpy array with all relevant values and uncertainties
- header:     title of rows/colums. Write as you would do in LaTeX. Name the uncertaintyof value 'omega' as 's_omega'
- fixerr:     if no header entry seems to be the suitable uncertainty, the uncertainty is taken from fixerr.
- showfixerr: set False to hide those uncertainties given by fixerr. Only certain digits will be shown.
- brackets:   use bracket or +- notation. e.g. 1.234(5) or 1.234+-0.005
- decimal:    use '.' or ',' as decimal separator
- separator:  use typically '|' or ' ' as column separator
- hor:        set True for horizontal tables instead of vertical
- selection:  preselect which entries to show. e.g. [0, 1, 5] will show columns with those indices. Detected uncertainties won't be shown separately anyway. <br>
e.g. \<!--- tab:val, fixerr=[.003, .1, .01, 0, 0], header=header -->
