# -*- coding: utf-8 -*-
"""
Created on Sat Oct 23 17:33:37 2021

@author: robin
"""
import numpy as np


def latex_matrix(*args, **kwargs):
    return latex_table(*args, form='pmatrix', **kwargs)

def latex_table(val, header=None, fixerr=None, showfixerr=True, brackets=True,
                decimal=',', separator='|', hor=False, selection=None,
                form='table'):
    '''
    val:        numpy array with all relevant 
                values and uncertainties
    header:     title of rows/colums. Write as you 
                would do in LaTeX. Name the uncertainty
                of value 'omega' as 's_omega'
    fixerr:     if no header entry seems to be the 
                suitable uncertainty, the uncertainty
                is taken from fixerr.
    showfixerr: set False to hide those uncertainties
                given by fixerr. Only certain digits
                will be shown.
    brackets:   use bracket or +- notation.
                e.g. 1.234(5) or 1.234+-0.005
    decimal:    use '.' or ',' as decimal separator
    separator:  use typically '|' or ' ' as column separator
    hor:        set True for horizontal tables instead of vertical
    selection:  preselect which entries to show.
                e.g. [0, 1, 5] will show columns with those indices.
                Detected uncertainties won't be shown separately anyway.
    '''
    #form:       'table' or any matrix command: 'matrix', 'pmatrix', ...
    
    arr = np.asarray(val)
    if form=='table':
        if type(header) is str and ',' in header:
            header = header.replace(', ', ',').replace(' ,', ',').split(',')
        elif type(header) is str and '&' in header:
            header = header.replace('& ', '&').replace(' &', '&').split('&')
    _a,_b = arr.shape
    if header is not None and _b!=len(header) and _a==len(header):
        arr = arr.transpose()
        _a,_b = arr.shape
    if fixerr is None:
        fixerr = [.01]*_b
    _err = {}
    if selection is None: selection = list(range(_b))
    if header is not None:
        header2 = [h.replace('$','').replace('{','').replace('}','') for h in header]
        for _i, _h in enumerate(header2):
            if 's_'+_h in header2:
                _i2 = header2.index('s_'+_h)
                _err[_i] = _i2
                #print('removed s_'+_h)
                try: selection.remove(_i2)
                except: pass
        header = [header[_i] for _i in selection]
    if form=='table':
        _s = '\\begin{table}\n  \\centering\n  \\begin{tabular}{'
        if hor and header is not None: _s += 'c|'
        if hor: _s += separator.join(['c']*_a)
        else: _s += separator.join(['c']*len(selection))
        _s += '}'
    else: _s = '\\begin{'+form+'}'
    _S1 = []
    for line in list(arr):
        _S2 = []
        for _i in selection:
            _v = line[_i]
            try: _S2 += [tovalid(_v, line[_err[_i]], latex=True, brackets=brackets)]
            except: _S2 += [tovalid(_v, fixerr[_i], latex=True, brackets=brackets, showerr=showfixerr)]
        _S1 += [_S2]
    
    if hor:
        if header is not None and form=='table':
            _S1 = [header] + _S1
        _c = range(len(_S1[0]))
        _s += '\\\\'.join(['\n    '+' & '.join([_s1[_i] for _s1 in _S1]) for _i in _c])
    else:
        if header is not None and form=='table':
            _s += '\n    '+' & '.join(header)+'\\\\\n    \\hline'
        _s += '\\\\'.join(['\n    '+' & '.join(_s2) for _s2 in _S1])
    if form=='table':
        _s += '\n  \\end{tabular}\n  \\caption{automatic generated table}\n  \\label{tab: any reference}\n\\end{table}'
    else:
        _s += '\n\\end{'+form+'}'
    return _s.replace('.', decimal)
def tovalid(val, err, latex=False, showerr=True, brackets=True):
    if latex: _pm = '\\pm'
    else: _pm = u'\u00B1'
    err = float(err)
    if showerr:
        n_digits = -int(np.log10(err)) + (err<1)
        if err*10**n_digits<3:
            n_digits += 1
        if brackets:
            return todigits(val, n_digits)+'('+str(int(err*10**n_digits))+')'
        return todigits(val, n_digits)+_pm+todigits(err, n_digits)
    n_digits = -int(np.log10(err))
    return todigits(val, n_digits)
def todigits(val, n_digits):
    if n_digits<=0:
        return str(int(round(val, n_digits)))
    _s = str(round(val, n_digits))
    #_s = '%0f'%round(val, n_digits)
    return _s + '0'*(n_digits-len(_s.split('.')[1]))
table = {}

if __name__ == '__main__':
    header = ['$A$', '$B$', '$\ell$', '$f$', '$s_f$']
    val = np.array([[0    , 5, 28.20,  7.96, .01],
                    [0    , 5, 28.62,  8.12, .02],
                    [0    ,10, 24.07, 12.49, .03],
                    [0    , 5, 45.80,  4.87, .06],
                    [0    , 5, 27.74,  8.09, .03],
                    [0    , 5, 44.43, 11.62, .04],
                    [0    , 3, 32.43,  4.40, .05],
                    [0    , 5, 37.12,  6.12, .03],
                    [0.615, 5, 37.66,  7.96, .12],
                    [0.615, 5, 30.49,  9.12, .03],
                    [0.615, 5, 31.93,  8.90, .02],
                    [0.615, 5, 38.89,  7.25, .02],
                    [0.615, 5, 38.59,  7.37, .05],
                    [1.025, 5, 42.68,  8.41, .07],
                    [1.025, 5, 37.86,  9.30, .08],
                    [1.605, 5, 37.74, 11.81, .02],
                    [1.605, 5, 67.95,  6.28, .04],
                    [1.605, 5, 27.52, 16.43, .03]])
    #table['all data'] = latex_table(val, fixerr=[.003, .1, .01, 0, 0], header=header)
    table['all data'] = latex_matrix(val, fixerr=[.003, .1, .01, 0, 0], header=header)
    print(table['all data'])