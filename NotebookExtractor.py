# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 16:09:56 2021
@author: robin

apply on .ipynk Notebooks to...
- convert formula between Markdown and Code within .ipynb
- convert Code sections to .py
- convert Markdown sections to .tex and insert numeric results, tables and figures
    
to convert a line, the line before must be respectively:
    ##convert               (in code)
    <!--- convert -->       (in markdown)
the converted lines will appear at the beginning of the next suitable cell

to include python results to markdown or LaTeX use this as a placeholder:
    <!--- val:VALUE -->
    <!--- rnd:VALUE, UNCERTAINTY -->
    <!--- tab:ARRAY [*ARGS] -->      (LaTeX conversion only)
    <!--- mat:ARRAY [*ARGS] -->      (LaTeX conversion only)
    figures will be saved and inserted anyway

e.g. <!--- tab:val, fixerr=[.003, .1, .01, 0, 0], header=header -->
for tab: and mat: ARGS press 't'
"""
import json, os
import traceback

from convert_formula import convert_formula
from latex_table import latex_table

myFile = 'sampleNotebook'
#myFile = 'AP1-V19'
myPath = 'Samples'

myData = None

def load(file=None):
    global myFile, myPath, myData
    if file not in [None, '']:
        myPath, myFile, e = anaFile(file)
    file = myPath+'/'+myFile+'.ipynb'
    with open(file, 'r') as _file:
        myData = json.load(_file)
    return myData

def save_ipynb(file=None, data=None):
    if file in [None, '']:
        file = myPath+'/'+myFile+'.ipynb'
    if data is None:
        data = myData
    if not '.' in file:
        file += '.ipynb'
    elif not file.endswith('.ipynb'):
        print("\n\nWARNING! better save with .ipynb file ending")
    if file == myFile:
        print("\n\ndon't override your sample File! It might become unusable")
        return
    print("saving to file %s..." % file)
    with open(file, 'w') as _file:
        json.dump(data, _file)

def save_py(file=None, code=None):
    global myFile, myPath, myData
    if file not in [None, '']:
        myPath, myFile, e = anaFile(file)
    file = myPath+'/'+myFile+'.py'
    if data is None:
        code = to_py(myData)
    print("saving to file %s..." % file)
    with open(file, 'w') as _file:
        _file.write(''.join(code))
    makepath(myPath+'figures')

def save_latex(file=None, mkdn=None):
    global myFile, myPath, myData
    if file not in [None, '']:
        myPath, myFile, e = anaFile(file)
    else:
        myPath += '/NBex_'+myFile
    file = myPath+'/'+myFile+'.tex'
    if data is None:
        mkdn = to_latex(myData)
    print("saving to file %s..." % file)
    with open(file, 'w') as _file:
        _file.write(''.join(mkdn))
        

def to_py(data, cell_separator=True):
    clines = []
    for cell in data['cells']:
        if cell['cell_type'] == 'code':
            if len(clines)>0:
                clines[-1] += '\n'
                if cell_separator:
                    clines += ['#########################################################\n']
            clines += cell['source']
    if 'import matplotlib.pyplot as plt\n' in clines:
        clines += ["\n\nfor i in plt.get_fignums():\n",
                  "    try:\n"
                  "        plt.figure(i)\n",
                  "        plt.savefig('figures/test_figure%d.png'%i)\n",
                  "        print('saved figure', i)\n",
                  "    except:\n",
                  "        print('could not save figure', i)"]
    #print(''.join(clines))
    return clines

fig_count = 0
def to_latex(data):
    global fig_count
    # get Sources
    mlines = []
    clines = []
    ID = []
    _no_idline = True
    for cnr, cell in enumerate(data['cells']):
        # get Markdown Source
        if cell['cell_type'] == 'markdown':
            source = cell['source']
            # replace headline markers ##
            for i,line in enumerate(source):
                if line.startswith('#### '): source[i] = '\\subsubsection{'+line[5:]+'}'
                elif line.startswith('### '): source[i] = '\\subsection{'+line[4:]+'}'
                elif line.startswith('## '): source[i] = '\\section{'+line[3:]+'}'
                elif line.startswith('# '): source[i] = '\\begin{center}\\Large\n    '+line[3:]+'\n\\end{center}'
                # replace $$
                if line.startswith('$$'): source[i] = '\\begin{equation}\\label{eq:eq%i}\n    '%i+line[2:]
                if line.endswith('$$'): source[i] = line[:-2]+'\n\\end{equation}'
            print(''.join(source))
            source = ''.join(source).split('<!---')
            # get Identifiers
            for i,s in enumerate(source[1:]):
                a, b = s.split('-->',1)
                a = a.replace(' ', '')
                if len(a.split(':'))!=2:
                    continue
                if any((a.startswith('val:'),
                        a.startswith('rnd:'),
                        a.startswith('mat:'),
                        a.startswith('tab:'))):
                    a = a[:4] + '%i:'%cnr + a[4:]
                    source[i+1] = a + '-->' + b
                    ID.append(a)
            mlines += '<!---'.join(source)
        # get Code source
        if cell['cell_type'] == 'code':
            if len(clines)>0:
                clines[-1] += '\n'
            clines += cell['source']
            # include image
            for cl in cell['source']:
                if 'plt.figure()' in cl:
                    fig_count +=1
                    mlines.append('\\begin{figure}[H]\\centering\n    \\includegraphics[width=1\\textwidth]{figure%i.png}\n    \\caption{}\n    \\label{fig:figure%i}\n\\end{figure}\n'%(fig_count,fig_count))
            # add value [...] grabber to Code
            if len(ID)>0:
                if _no_idline:
                    clines.append('\n\nfrom latex_table import latex_table, latex_matrix, tovalid\n')
                    clines.append('__latex_export = {}\n')
                    _no_idline = False
                for a in ID:
                    cd = a.split(':',3)[2]
                    if a.startswith('rnd:'):
                        cd = 'tovalid('+cd+')'
                    elif a.startswith('mat:'):
                        cd = 'latex_matrix('+cd+')'
                    elif a.startswith('tab:'):
                        cd = 'latex_table('+cd+')'
                    clines.append('try: __latex_export["%s"] = %s\nexcept:\n    print("could not get %s")\n    __latex_export["%s"] = "<!--- ERROR -->"\n'%(a, cd, a, a))
                ID = []
                clines += ['#########################################################\n']
    # add figure grabber to Code
    if 'import matplotlib.pyplot as plt\n' in clines:
        clines += ["for i in plt.get_fignums():\n",
                  "    try:\n"
                  "        plt.figure(i)\n",
                  "        plt.savefig(%s'figure%%i.png'%%i)\n"%myPath,
                  "        print('saved figure', i)\n",
                  "    except:\n",
                  "        print('could not save figure', i)"]
    # save python file
    print("saving to file workingfile.py...")
    with open('workingfile.py', 'w') as _file:
        _file.write(''.join(clines))
    # get Data from evaluated Code
    print('\n##################\nimport workingfile...\n')
    try:
        import workingfile as py
        print('\n...import done\n###############\n')
    except:
        traceback.print_exc()
        print('\n...import failed\n#################\n')
        return
    # insert Data
    mlines = ''.join(mlines).split('<!---')
    for i,s in enumerate(mlines[1:]):
        a, b = s.split('-->',1)
        if 'convert' in a: a=''
        else:
            try:
                a = str(py.__latex_export[a])
            except KeyError:
                a = '<!--- ERROR -->'
        mlines[i+1] = a + b
    return ''.join(mlines)

def makepath(path):
    '''ensures a given directory exists'''
    if not os.path.exists(path):
        os.makedirs(path)

def anaFile(file):
    '''
    returns:
        path, name, ending
    '''
    file = file.split('/')
    name, ending = file[-1].split('.')
    return '/'.join(file[:-1]), name, ending


if __name__ == '__main__':
    if True:
        print('This software can convert Jupyter Notebooks.')
        converted = False
        import sys
        while True:
            try:
                file = input('enter File name:\ndefault: %s\n'%myFile)
                if file in ['q','quit','close','x','end','exit']:
                    sys.exit(0)
                data = load(file)
                break
            except (ValueError, FileNotFoundError):
                print('File not Found')
        myData = data
        print(myData)
        while True:
            task = input('options\n    c:  convert formula\n    p:  extract Python code\n    L:  extract LaTeX code\n    s:  save...\n    o:  load other Notebook\n    q:  close\n    h:  help\n')
            task = task.lower()
            if task in ['o','open','load']:
                try:
                    file = input('enter File name:\ndefault:'+myPath+'/'+myFile+'.ipynb')
                    if file in ['q','quit','close','x','end','exit']:
                        sys.exit(0)
                    myData = load(file)
                    converted = False
                    myCode = None
                    myLatex = None
                    print(myData)
                except:
                    print('File not Found')
            elif task in ['?','h','help']:
                task = input(__doc__)
                if task == 't':
                    input(latex_table.__doc__)
            elif task in ['q','quit','close','x','end']:
                break
            elif task in ['c','convert','formula']:
                myData = convert_formula(myData)
                print(myData)
                converted = True
            elif task in ['p','py','python','code']:
                myCode = to_py(myData)
                print(myCode)
            elif task in ['l','latex','tex','markdown']:
                myLatex = to_latex(myData)
                print(myLatex)
            elif task in ['s', 'save']:
                task = input('What Format should be saved?\n    c:  converted formula\n    p:  Python code\n    L:  LaTeX code\n')
                task = 's'+task.lower()
            if task == 'sc':
                file = input('enter Jupyter File name:\n')
                if file == '': 
                    p,n,e = anaFile(myFile)
                    file = p+'/result_'+n+'.'+e
                if not converted:
                    myData = convert_formula(myData)
                save_ipynb(file, myData)
            elif task == 'sp':
                file = input('enter Python File name:\n')
                if file == '': file = myFile
                save_py(file, to_py(myData))
            elif task == 'sl':
                file = input('enter LaTeX File name:\n')
                if file == '': file = myFile
                save_latex(file, to_latex(myData))
                
    
    else:
        data = load(myFile)
        
        #to_latex(data)
        
        lines = to_py(data)
        #print(''.join(lines))
        save_py(myFile, lines)
        
        #print(data)
        #save_ipynb('result_'+myFile, data)