# -*- coding: utf-8 -*-
"""
Created on Sat Oct 23 18:17:11 2021

@author: robin
"""

def convert_formula(data):
    code_form = []
    mkdn_form = []
    for i,cell in enumerate(data['cells']):
        convert_flag = False
        if cell['cell_type'] == 'code':
            for line in cell['source']:
                if convert_flag:
                    print(line)
                    mkdn_form.append(_to_mkdn(line.strip())+'\n')
                    convert_flag = False
                elif line == '##convert\n':
                    convert_flag = True
            if len(code_form)>0:
                data['cells'][i]['source'] = (['##automatically converted formula\n']
                                              +code_form+['##\n','\n']+cell['source'])
                code_form = []
        elif cell['cell_type'] == 'markdown':
            for line in cell['source']:
                if convert_flag:
                    print(line)
                    if line.startswith('$'):
                        code_form.append(_to_code(line)+'\n')
                    convert_flag = False
                elif line == '<!--- convert -->\n':
                    convert_flag = True
            if len(mkdn_form)>0:
                data['cells'][i]['source'] = (['<!--- automatically converted formula -->\n']
                                              +mkdn_form+['<!--- -->\n','\n']+cell['source'])
                mkdn_form = []
    if len(code_form)>0:
        data['cells'].append({"cell_type": "code",
                              'source': (['##automatically converted formula\n']
                                         +code_form+['##'])})
    if len(mkdn_form)>0:
        data['cells'].append({"cell_type": "markdown",
                              'source': (['<!--- automatically converted formula -->\n']
                                         +mkdn_form+['<!--- -->'])})
    return data
            
trans = {'alpha':'\\alpha',
         'beta':'\\beta',
         'gamma':'\\gamma',
         'delta':'\\delta',
         'del':'\\partial',
         'eps':'\\varepsilon',
         'epsilon':'\\epsilon',
         'zeta':'\\zeta',
         'eta':'\\eta',
         'theta':'\\vartheta',
         'kappa':'\\kappa',
         'lam':'\\lambda',
         'mu':'\\mu',
         'my':'\\mu',
         'nu':'\\nu',
         'ny':'\\nu',
         'xi':'\\xi',
         'omikron':'\\omikron',
         'pi':'\\pi',
         'rho':'\\rho',
         'sigma':'\\sigma',
         'tau':'\\tau',
         'ypsilon':'\\ypsilon',
         'phi':'\\varphi',
         'chi':'\\chi',
         'psi':'\\psi',
         'omega':'\\omega',
         'Alpha':'\\Alpha',
         'Beta':'\\Beta',
         'Gamma':'\\Gamma',
         'Delta':'\\Delta',
         'Epsilon':'\\Epsilon',
         'Zeta':'\\Zeta',
         'Eta':'\\Eta',
         'Theta':'\\Theta',
         'Kappa':'\\Kappa',
         'Lambda':'\\Lambda',
         'My':'\\Mu',
         'Mu':'\\Mu',
         'Ny':'\\Nu',
         'Nu':'\\Nu',
         'Xi':'\\Xi',
         'Omikron':'\\Omikron',
         'Pi':'\\Pi',
         'Rho':'\\Rho',
         'Sigma':'\\Sigma',
         'Tau':'\\Tau',
         'Ypsilon':'\\Ypsilon',
         'Phi':'\\Phi',
         'Chi':'\\Chi',
         'Psi':'\\psi',
         'Omega':'\\omega',
         
         'np.pi':'\\pi',
         'np.sqrt':'\\sqrt',
         'np.exp':'\\exp',
         'np.log':'\\ln',
         'np.log10':'\\log',
         'np.sin':'\\sin',
         'np.arcsin':'\\arcsin',
         'np.cos':'\\cos',
         'np.arccos':'\\arccos',
         'np.tan':'\\tan',
         'np.arctan':'\\arctan',
         'sum(':'\\Sigma_{n=1}^{N}{',
         '**':'^',
         '||':'\|'
         }
rtrans = {v: k for k, v in trans.items()}
rtrans['\\cdot'] = ' * '
rtrans['\\theta'] = 'theta'
rtrans['\\phi'] = 'phi'
rtrans['\\left'] = ''
rtrans['\\right'] = ''
rtrans['*\\frac'] = ''
rtrans['*\\dfrac'] = ''
rtrans['\\{'] = '('
rtrans['\\}'] = ')'
#rtrans['\\\\'] = '\n' # schlecht in Matrix
rtrans['<br>'] = '\n'
trans['*'] = ''

#print(rtrans)

def translate(s, dic):
    for k, v in dic.items():
        s = s.replace(k, v)
    return s

def _to_mkdn(code):
    if code.endswith('\n'): code = code[:-1]
    
    #return 'converted(%s)'%code
    code = code.replace('np.pi', '\\pi')
    code = code.replace(' * ', ';~\\cdot~;')
    code = code.replace(' / ', ';##frac;')
    code = code.replace('/(', ';##frac;(')
    code = code.replace(')/', ');##frac;')
    code = code.replace(' - ', ';~-~;')
    code = code.replace(' + ', ';~+~;')
    code = code.replace('^', ';^;')
    code = code.replace('**', ';^;')
    code = code.replace('*', ';*;')
    code = code.replace('/', ';/;')
    code = code.replace('+', ';+;')
    code = code.replace('-', ';-;')
    code = code.replace('(', ';(;')
    code = code.replace(')', ';);')
    code = code.replace('\\', ';\\')
    code = code.replace(' ', ';')
    code = code.replace(';;;', ';')
    code = code.replace(';;', ';')
    code = code.replace(';;', ';')
    code = code.replace('~', ' ')
    code = code.replace('np.array', ' ')
    #print(code)
    words = code.split(';')
    level = [0]*len(words)
    L = 0
    fracindex = []
    for i, word in enumerate(words):
        if word == '(': L +=1
        elif word == ')': L -=1
        level[i] = L
    #for k,v in zip(level, words):
    #    print(k, v)
        
        # deal with fractions
    for i, word in enumerate(words):
        if word == '##frac':
            #print('  detected ##frac:', words[i-1])
            if words[i-1] == ')':
                #print('  revrange', list(range(i-1, 0, -1)))
                for j in range(i-1, 0, -1):
                    if level[j-1]==L and words[j]=='(':
                        fracindex.append(j)
                        break
            else:
                fracindex.append(i-1)
        elif word.startswith('[[') and word.endswith(']]'):
            if word.count('[') > word.count(']'):
                words[i+1] = words[i] + words[i+1]
                words[i] = ''
                continue
            words[i] = words[i].replace('],[','\\\\')
            words[i] = words[i].replace(',','&')
            words[i] = '\\begin{matrix}'+words[i][2:-2]+'\\end{matrix}'
        elif word.startswith('[') and word.endswith(']'):
            words[i] =  words[i].replace(',','\\\\')
            words[i] = '\\begin{matrix}'+words[i][1:-1]+'\\end{matrix}'
            
    fracindex.reverse()
    for index in fracindex:
        words.insert(index, '\\frac')
    
    # set bracket types {} ()
    #    {} after _
    #    {} for frac
    #    {} for sqrt
    #    \left(\right) after function, and when no obvios reason -> default
    prev = None
    for i, word in enumerate(words):
        if word == '(':
            #print(' replace', prev, word)
            if prev in ['\\frac','##frac','np.sqrt']:
                # replace both brackets with {}
                words[i] = '{'
                for j in range(i+1, len(words)-1):
                    if level[j]==L and words[j]==')':
                        words[j] = '}'
                        break
            else:
                # replace both brackets with \left(\right)
                words[i] = '\\left('
                for j in range(i+1, len(words)-1):
                    if level[j]==L and words[j]==')':
                        words[j] = '\\right)'
                        break
        elif prev == '^' and len(word)>1 and word[0]!='\\':
            words[i] = '{'+word+'}'
        else:
            words[i] = words[i].replace('_','_{')
            words[i] = words[i]+('}'*word.count('_'))
        prev = word
    code = ''.join(words)
    code = code.replace('##frac','')
    return '$$'+translate(code, trans)+'$$'



functions = ['\\sqrt',
             '\\exp',
             '\\log',
             '\\ln',
             '\\sin',
             '\\cos',
             '\\tan',
             '\\arcsin',
             '\\arccos',
             '\\arctan']
signs = '+-*/=><|&%^_"{}[]()~, '
nr = '.,0123456789'



def _to_code(mkdn):
    if mkdn.endswith('\n'): mkdn = mkdn[:-1]
    mkdn = mkdn.strip('$ ')
    mkdn = mkdn.replace('\\cdot',' * ')
    mkdn = mkdn.replace('\\text','"')
    mkdn = mkdn.replace('\\text','"')
    mkdn = mkdn.replace('[','{')
    mkdn = mkdn.replace(']','}')
    mkdn = mkdn.replace(',','.')
    mkdn = mkdn.replace('\\begin{matrix}','\\#mat ')
    mkdn = mkdn.replace('\\begin{pmatrix}','\\#mat ')
    mkdn = mkdn.replace('\\begin{bmatrix}','\\#mat ')
    mkdn = mkdn.replace('\\begin{Bmatrix}','\\#mat ')
    mkdn = mkdn.replace('\\begin{vmatrix}','\\#mat ')
    mkdn = mkdn.replace('\\begin{Vmatrix}','\\#mat ')
    mkdn = mkdn.replace('\\end{matrix}','\\mat# ')
    mkdn = mkdn.replace('\\end{pmatrix}','\\mat# ')
    mkdn = mkdn.replace('\\end{bmatrix}','\\mat# ')
    mkdn = mkdn.replace('\\end{Bmatrix}','\\mat# ')
    mkdn = mkdn.replace('\\end{vmatrix}','\\mat# ')
    mkdn = mkdn.replace('\\end{Vmatrix}','\\mat# ')
    
    # group words
    words = []
    level = []
    L = 0
    iscommand = False
    isnr = False
    issub = False
    prev = None
    for char in mkdn:
        if char in ['}']: L -=1
        if issub:
            #print(L, char, prev, words[-1], L==Lsub, char not in '{', prev not in '_" ')
            if L==Lsub and char not in '{' and prev not in '_" ':
                issub = False
            if char not in '{}':
                words[-1] += char
            
        elif char in '_"':
            if char == '_': words[-1] += char
            else:
                words.append('')
                level.append(L)
            iscommand = False
            isnr = False
            issub = True
            Lsub = L
        
        elif char in nr:
            #print('is nr')
            if isnr:
                words[-1] += char
            else:
                words.append(char)
                level.append(L)
                isnr = True
                iscommand = False
        elif char == '\\':
            #print('is command')
            words.append(char)
            level.append(L)
            iscommand = True
            isnr = False
        elif char == ' ':
            words[-1] += char
            iscommand = False
            isnr = False
        elif char in signs:
            words.append(char)
            level.append(L)
            iscommand = False
            isnr = False
        elif iscommand:
            words[-1] += char
        else:
            words.append(char)
            level.append(L)
        if char in ['{']: L +=1
        prev = char
    #for k,v in zip(level, words):
    #    print(k, v)
        # deal with standard functions
    for i, word in enumerate(words[:-1]):
        if (any(word.startswith(fun) for fun in functions)
              and words[i+1][0] not in '{[('):
            words[i] = word.strip()
            words[i+1] = '('+words[i+1].strip()+')'
    ismat = False
    # deal with \frac an matrices
    for i, word in enumerate(words[:-2]):
        if word.startswith('\\frac') or word.startswith('\\dfrac'):
            L = level[i]
            #print('level =', L, 'from', i, '- search in', words[i+1:-1])
            for j in range(i+1, len(words)-1):
                if level[j]==L and not words[j].startswith('{'):
                    words[j+1] = '/'+words[j+1]
                    break
        elif word.startswith('\\#mat '):
            words[i] = 'np.array([['
            ismat=True
        elif word.startswith('\\mat# '):
            words[i] = ']]) '
            ismat=False
        elif ismat:
            words[i] = word.replace('&',',')
            if word.startswith('\\') and words[i-1] == '\\':
                words[i-1] = '~'
                words[i] = '],['
        # * between characters, except signs, in commands, and into brackets
    for i, word in enumerate(words[:-1]):
        if ((word[0] not in signs and words[i+1][0] not in signs and not word=='np.array([[')
            or (word[0] in ')}' and words[i+1][0] not in signs)
            or (word[0] not in signs and words[i+1][0] in '{(' and not any(word.startswith(fun) for fun in functions))):
            #print(word+'[0] not in signs')
            words[i+1] = '*'+words[i+1]
    #print(words)
    
    mkdn = ''.join(words).replace('~','')
    
    return translate(mkdn, rtrans)
    
if __name__ == '__main__':
    print(_to_code('$$ \\alpha_b - \\varphi_{abg}\\begin{pmatrix}0&1\\\\1&0\\end{pmatrix}\\cdot A^3.67\\frac{\\text{B}\\sqrt c}\\Sigma(a+b)$$'))
    print(_to_mkdn('alpha_b - phi_abg*np.array([[0,1],[1,0]]) *  A**3.67*(B*np.sqrt(c))/Sigma*(a+b)'))
    print('\n$$ \\alpha_b - \\varphi_{abg}\\begin{pmatrix}0&1\\\\1&0\\end{pmatrix}\\cdot A^3.67\\frac{\\text{B}\\sqrt c}\\Sigma(a+b)$$')